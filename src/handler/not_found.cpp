#include "handler.hpp"

using namespace HTTP;

Response NotFoundHandler::handle(Call&, const VirtualHost&) const
{
	Response response;
	response.setResponseCode(ResponseCode::NOT_FOUND);
	response.setBody("<html><head><title>404 - Not Found</title></head><body><h1>Not Found</h1><p>Woops, it looks like the page you asked for was not found :/</p></body></html>");

	return response;
}
