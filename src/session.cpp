#include "http.hpp"

#include<uuid/uuid.h>

using namespace HTTP;

string HTTP::genUUID()
{
	uuid_t uuid;
	uuid_generate(uuid);

	char s[36];
	uuid_unparse(uuid, s);

	return string(s);
}

const string* Session::getData(const string&& name) const
{
	const auto search = data.find(name);
	return (search != data.end() ? &(search->second) : nullptr);
}
