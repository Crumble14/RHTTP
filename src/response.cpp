#include "http.hpp"

#include<algorithm>

using namespace HTTP;

string HTTP::getDate()
{
	time_t now;
	tm ts;
	std::array<char, 80> buf;

	time(&now);
	ts = *localtime(&now);
	strftime(buf.data(), buf.size(), "%a, %d %b %Y %H:%M:%S %Z", &ts);

	return string{buf.data()};
}

string HTTP::responseCodeStr(const ResponseCode code)
{
	const auto search = responseCodes.find(code);
	return (search != responseCodes.end() ? search->second : "Unknown");
}

string HTTP::getMimeType(const string& extention)
{
	const auto search = extention_mimeTypes.find(extention);
	return (search != extention_mimeTypes.end() ? search->second : "text/plain");
}

const string* Response::getHTTPParameter(const string&& key) const
{
	auto search = httpParameters.find(key);
	return (search != httpParameters.end() ? &search->second : nullptr);
}

void Response::setHTTPParameter(const string&& key, const string& value)
{
	httpParameters[key] = value;
}

string Response::toString(const bool content)
{
	if(!getHTTPParameter("Server")) setHTTPParameter("Server", "RHTTP");

	setHTTPParameter("Connection", "close");
	setHTTPParameter("Date", getDate());

	string b = body;

	if(content) {
		for(const auto& r : replacements) {
			str_replace(b, "${" + r.first + '}', r.second);
		}

		setHTTPParameter("Content-Length", to_string(b.size()));
	}

	string s = "HTTP/1.1 " + to_string(responseCode) + ' ' + responseCodeStr(responseCode) + '\n';

	for(const auto& p : httpParameters) {
		s += p.first + ": " + p.second + '\n';
	}

	for(const auto& c : cookies) {
		s += "Set-Cookie: " + c.first + '=' + c.second + "; Path=/" + '\n';
	}

	s += "\r\n";
	s += b;

	return s;
}
